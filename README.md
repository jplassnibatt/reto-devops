# Requisitos:
```
make
docker
docker-compose
minikube
helm
```

# Reto 1

Crea imagen Docker:
```sh
$ docker build -t mynodejslocal:v1 .
```

Lanza contenedor:
```sh
$ docker run -d -p 3000:3000 --name mynodejs mynodejslocal:v1
```

Verificar servicio y que el user no es root:
```sh
$ curl localhost:3000
$ docker exec -it mynodejs sh
$ whoami
$ exit
```

# Reto 2

Se habilita registry en Docker para trabajar con repositorio local:
```sh
$ docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

Se agrega tag para usar imagen de repositorio local:
```sh
$ docker tag mynodejslocal:v1 localhost:5000/mynodejslocal:v1
```

Se crea imagen propia de Nginx:
```sh
$ docker build -t mynginx:v1 ./mynginx/
```

Se agrega tag para usar imagen de repositorio local:
```sh
$ docker tag mynginx:v1 localhost:5000/mynginx:v1
```
Lanza docker-compose:
```sh
$ docker-compose up -d
```

Verificar redirección a https en http://localhost.

Verificar auth `admin:admin` en http://localhost/private.

# Reto 3

Simple build y test de la aplicación nodejs en [Gitlab](https://gitlab.com/jplassnibatt/reto-devops/-/pipelines).

# Reto 4

Se configura Minikube para habilitar registro local:
```sh
$ minikube addons enable registry
$ minikube addons enable ingress
```

Se crea imagen en registro de Minikube:
```sh
$ eval $(minikube -p minikube docker-env)
$ docker build . -t localhost:5000/mynodek8s:v1
$ docker build ./mynginxk8s -t localhost:5000/mynginxk8s:v1
$ unset DOCKER_TLS_VERIFY
$ unset DOCKER_HOST
$ unset DOCKER_CERT_PATH
```

Lanza el despliegue de Kubernetes en Minikube:
```sh
$ kubectl apply -f ./k8s/mynodek8s.deployment.yaml
$ minikube service list
```
Revisar el servicio y conectarse de acuerdo a la información del comando anterior.

# Reto 5
```sh
$ helm install mynodehelm mynodehelm/
$ minikube service list
```

Revisar el servicio y conectarse de acuerdo a la información del comando anterior.

# Reto 7

Simple Makefile con comandos por `reto` en la raíz del proyecto.

```sh
$ make
reto1-build_docker          reto2-start_docker-compose  reto4-build_k8s             reto5-start_helm
reto1-start_docker          reto2-stop_docker-compose   reto4-start_k8s             reto5-stop_helm
reto1-stop_docker           reto3-start_cicd            reto4-stop_k8s
```
