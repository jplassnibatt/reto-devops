reto1-build_docker:
	@clear
	@echo "--------------Construyendo la imagen de docker de Nodejs--------------"
	@echo ""
	@docker build -t mynodejslocal:v1 .

reto1-start_docker:
	@clear
	@echo "-------------------------Lanza docker de Nodejs-----------------------"
	@echo ""
	@docker run -d -p 3000:3000 --name mynodejs mynodejslocal:v1
	@echo ""
	@echo "--------------------Verificar user distinto a root--------------------"
	@echo "---------------------$$ docker exec -it mynodejs sh---------------------"
	@echo "---------------------$$ whoami------------------------------------------"
	@echo "---------------------$$ exit--------------------------------------------"

reto1-stop_docker:
	@clear
	@echo "-------------Deteniendo el contenedor de docker de Nodejs-------------"
	@echo ""
	@docker stop mynodejs
	@echo ""
	@echo "-------------Eliminando el contenedor de docker de Nodejs-------------"
	@echo ""
	@docker rm mynodejs

reto2-start_docker-compose:
	@clear
	@echo "---------------Se aplica tag para usar imagen localmente--------------"
	@docker tag mynodejslocal:v1 localhost:5000/mynodejslocal:v1
	@echo ""
	@echo "-----------------Se creará una imagen propia de nginx-----------------"
	@echo ""
	@docker build -t mynginx:v1 ./mynginx/
	@echo ""
	@echo "---------------Se aplica tag para usar imagen localmente--------------"
	@echo ""
	@docker tag mynginx:v1 localhost:5000/mynginx:v1
	@echo "---------------------------Lanza docker-compose-----------------------"
	@echo ""
	@docker-compose up -d
	@echo ""
	@echo "---------------Verifica aplicación http, https y auth-----------------"
	@echo "---------------------------------http---------------------------------"
	@echo "--------------------------$$ curl localhost----------------------------"
	@echo ""
	@echo "---------------------------------https--------------------------------"
	@echo "--------------------$$ curl -k https://localhost-----------------------"
	@echo ""
	@echo "---------------------------auth admin:admin---------------------------"
	@echo "------------------$$ curl http://localhost/private---------------------"

reto2-stop_docker-compose:
	@clear
	@echo "-------------------Detiene docker-compose-------------------"
	@docker-compose stop
	@docker-compose down

reto3-start_cicd:
	@clear
	@echo "---------------------Ve al repositorio en gitlab----------------------"

reto4-build_k8s:
	@clear
	@echo "------------Configura minikube para utilizar registro local-----------"
	@minikube addons enable registry
	@minikube addons enable ingress
	@echo ""
	@echo "-----------------Crea imagen en registro de minikube------------------"
	@eval $$(minikube -p minikube docker-env) ;\
	docker build . -t localhost:5000/mynodek8s:v1
	@eval $$(minikube -p minikube docker-env) ;\
	docker build ./mynginxk8s -t localhost:5000/mynginxk8s:v1

reto4-start_k8s:
	@clear
	@echo "-------------------------Lanza despliegue k8s-------------------------"
	@kubectl apply -f ./k8s/mynodek8s.deployment.yaml
	@minikube service list
	@echo "------------Basado en la información, revisar servicio http-----------"

reto4-stop_k8s:
	@clear
	@echo "------------------------Detiene despliegue k8s------------------------"
	@kubectl delete service,deployment mynodek8s
	@kubectl delete horizontalpodautoscalers mynodek8s-hpa

reto5-start_helm:
	@clear
	@echo "-------------------------Lanza despliegue helm------------------------"
	@helm install mynodehelm mynodehelm/
	@minikube service list
	@echo "--------Basado en la información, revisar servicio http y https-------"

reto5-stop_helm:
	@clear
	@echo "------------------------Detiene despliegue helm-----------------------"
	@helm uninstall mynodehelm
