#imagen base
FROM node:16-alpine
#set directorio para usar user node
WORKDIR /home/node
#copia archivos de aplicación y cambio permisos
COPY --chown=node:node package*.json index.js ./
#cambia a user node
USER node
#instala
RUN npm ci --only=production
#expone puerto
EXPOSE 3000
#lanza applicación
CMD ["node", "index.js"]
